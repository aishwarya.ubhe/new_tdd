class CreatePatients < ActiveRecord::Migration[6.0]
  def change
    create_table :patients do |t|
      t.string :name
      t.string :gender
      t.string :email
      t.string :age

      t.timestamps
    end
  end
end
