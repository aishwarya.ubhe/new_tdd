class CreateDoctors < ActiveRecord::Migration[6.0]
  def change
    create_table :doctors do |t|
      t.string :name
      t.integer :experience
      t.integer :fee
      t.boolean :availability

      t.timestamps
    end
  end
end
