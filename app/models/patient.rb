class Patient < ApplicationRecord
  validates :name ,presence: true
  validates :gender ,presence:false
  validates :email ,presence: true
  validates :age ,presence: false
end
