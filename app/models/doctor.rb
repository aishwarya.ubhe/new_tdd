class Doctor < ApplicationRecord
  scope :available_doctors, -> { where(availability: true) }
  scope :unavailable_doctors, -> { where(availability: false) }

  validates :name ,presence: true
  validates :experience ,presence: true
  validates :fee ,presence: true
end
