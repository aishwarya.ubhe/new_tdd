Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root 'doctors#index'
  #resources :doctors, only:[:index, :show]
  resources :doctors do
    member do
      get :delete
    end
  end

  resources :patients
  # do
  #   member do
  #     get :delete
  #   end
  # end
end
