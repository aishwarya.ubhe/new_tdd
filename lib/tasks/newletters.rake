namespace :newletters do
  desc "send newletters"
  task from_bridge: :environment do
    SendFromBridgeNewletterJob.perform_later
  end
end
