namespace :patient do
  desc "Patient details"
  task create: :environment do
    patient = Patient.create!(name: 'Nakita', gender:'Female', email: 'abc@abc.com', age:'50')
    puts "Created Patient"
  end

  task show: :environment do
    patient = Patient.find(1)
    puts "Show: #{patient.name}, #{patient.gender}, #{patient.email}"
  end

  task edit: :environment do
    patient = Patient.find(1)
    puts "Show: #{patient.name}, #{patient.gender}, #{patient.email},#{patient.age}"
    patient = Patient.update(email: '3000')
    puts "Edited patient details"
    #puts "#{patient.email}"
  end

  task delete: :environment do
    patient = Patient.find(4)
    patient.destroy
    puts "Deleted patient"
  end
end