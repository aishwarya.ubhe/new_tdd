namespace :doctor do
  desc "Doctor details"
  task create: :environment do
    doctor = Doctor.create!(name: 'John Peter', experience: '15', fee: '1000')
    puts "Created Doctor"
  end

  task show: :environment do
    doctor = Doctor.find(17)
    puts "Show: #{doctor.name}, #{doctor.experience}, #{doctor.fee}"
  end

  task edit: :environment do
    doctor = Doctor.find(17)
    puts "Show: #{doctor.name}, #{doctor.experience}, #{doctor.fee}"
    doctor = Doctor.update(fee: '3000')
    puts "Edited doctor details"
  #  puts "Show:#{doctor.fee}"
  end

  task delete: :environment do
    doctor = Doctor.find(13)
    doctor.destroy
    puts "Deleted doctor"
  end
end