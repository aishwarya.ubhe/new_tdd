require 'rails_helper'

RSpec.describe "from_bridge:" do
  before do
    ActiveJob::Base.queue_adapter.enqueued_jobs.clear
  end
  it "schedules bg job to perform soon" do
    subject.execute
    expect(ActiveJob::Base.queue_adapter.enqueue_jobs.size).to eq 1
  end
end