require 'rails_helper'

RSpec.describe DoctorsController, "#create" do
  context "GET #index" do
    it "return a success response" do
      get :index
      expect(response.successful?)
    end
  end

  context 'GET #show' do
    it 'returns a success response' do
      doctor = Doctor.create!(name: 'xyz',experience:'5',fee: '100').save
      get :show, params: { id: doctor.to_param }
      expect(response.successful?)
    end
  end
end