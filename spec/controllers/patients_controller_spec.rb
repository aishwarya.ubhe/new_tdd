require 'rails_helper'

RSpec.describe PatientsController, "#create" do
  context "GET #index" do
    it "return a success response" do
      get :index
      expect(response.successful?)
    end
  end

  context 'GET #show' do
    it 'returns a success response' do
      patient = Patient.create!(name: 'xyz',gender:'male',email: 'abc@abc.com',age: '20').save
      get :show, params: { id: patient.to_param }
      expect(response.successful?)
    end
  end
end