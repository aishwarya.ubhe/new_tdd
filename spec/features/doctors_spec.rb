require 'rails_helper'

RSpec.feature "Doctors", type: :feature do
  context 'create new doctor' do
    before(:each) do
      visit new_doctor_path
      within('form') do
        fill_in 'Fee', with: '300'
      end
    end
    scenario "should be successful" do
      visit new_doctor_path
      within('form') do
        fill_in 'Name', with: 'Batra'
        fill_in 'Experience', with: '10'
      end
      click_button 'Create Doctor'
      expect(page).to have_content("Doctor")
    end

    scenario 'should fail when name blank' do
      click_button 'Create Doctor'
      expect(page).to have_content("Name can't be")
    end

    scenario 'should fail when Experience blank' do
      click_button 'Create Doctor'
      expect(page).to have_content("Experience can't be")
    end
  end

  context 'update doctor' do
    let!(:doctor){Doctor.create(name: 'Batra', experience: '10', fee: '300')}
    before(:each) do
      visit edit_doctor_path(doctor)
    end
    scenario "should be successful" do
      within('form') do
        fill_in 'Fee', with: '300'
      end
      click_button 'Edit Doctor'
      expect(page).to have_content("Doctor")
      expect(page).to have_content("Batra")
    end

    scenario "should fail when name blank" do
      within('form') do
        fill_in 'Name', with: ''
      end
      click_button 'Edit Doctor'
      expect(page).to have_content("Name can't be blank")
    end

    scenario "should fail when Experience blank" do
      within('form') do
        fill_in 'Experience', with: ''
      end
      click_button 'Edit Doctor'
      expect(page).to have_content("Experience can't be blank")
    end
  end

  context 'destroy doctor' do
    scenario "should be successful" do
      doctor = Doctor.create(name: 'Batra', experience: '10', fee: '300')
      visit doctors_path
      expect{ click_link 'Delete' }.to change(Doctor, :count).by(-1)
      expect(page).to have_content("Doctor")
    end
  end
end
