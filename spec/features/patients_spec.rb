require 'rails_helper'

RSpec.feature "Patients", type: :feature do
  context 'create new patient' do
    before(:each) do
      visit new_patient_path
      within('form') do
        fill_in 'Gender', with: 'Male'
        fill_in 'Age', with: '30'
      end
    end
    scenario "should be successful" do
      visit new_patient_path
      within('form') do
        fill_in 'Name', with: 'Batra'
        fill_in 'Email', with: 'abc@abc.com'
      end
      click_button 'Create Patient'
      expect(page).to have_content("Patient")
    end

    scenario 'should fail when name blank' do
      click_button 'Create Patient'
      expect(page).to have_content("Name can\'t be")
    end

    scenario 'should fail when Email blank' do
      click_button 'Create Patient'
      expect(page).to have_content("Email can\'t be")
    end
  end

  context 'update patient' do
    let!(:patient){Patient.create(name: 'Batra', gender:'Male', email: 'abc@abc.com', age:'30')}
    before(:each) do
      visit edit_patient_path(patient)
    end
    scenario "should be successful" do
      within('form') do
        fill_in 'Gender', with: 'Male'
        fill_in 'Age', with: '30'
      end
      click_button 'Edit Patient'
      expect(page).to have_content("Patient")
      expect(page).to have_content("abc@abc.com")
    end

    scenario "should fail when name blank" do
      within('form') do
        fill_in 'Name', with: ''
      end
      click_button 'Edit Patient'
      expect(page).to have_content("Name can\'t be blank")
    end

    scenario "should fail when Email blank" do
      within('form') do
        fill_in 'Email', with: ''
      end
      click_button 'Edit Patient'
      expect(page).to have_content("Email can\'t be blank")
    end
  end

  context 'destroy patient' do
    scenario "should be successful" do
      patient = Patient.create(name: 'Batra', gender:'Male', email: 'abc@abc.com', age:'30')
      visit patients_path
      expect{ click_link 'Delete' }.to change(Patient, :count).by(-1)
      expect(page).to have_content("Patient")
    end
  end
end
