require "rails_helper"

RSpec.describe "patients/index" do
  it "displays all the patients" do
    assign(:patients, [
      Patient.create!(:name => 'xyz',:gender =>'Male', :email => 'abc@abc.com',:age =>'20')
    ])

    render

    expect(rendered).to match /xyz/
    expect(rendered).to match /Male/
    expect(rendered).to match /abc@abc.com/
    expect(rendered).to match /20/
  end
end