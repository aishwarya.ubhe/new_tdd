require "rails_helper"

  RSpec.describe "patients/show" do
    it "displays patient details correctly" do
      patient = Patient.create!(:name => 'xyz',:gender =>'Male', :email => 'abc@abc.com',:age =>'20')
      controller.extra_params = { :id => patient.id }
      expect(controller.request.fullpath).to eq patient_path(patient)
    end
end