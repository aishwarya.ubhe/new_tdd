# require 'spec_helper'

# RSpec.describe 'doctors/show.html.erb' do
#   it 'displays doctors details correctly' do
#     assign(:doctors, Doctor.create(name: 'xyz',experience:'5',fee: '100'))
#     render
#     rendered.should contain('xyz')
#     rendered.should contain('5')
#     rendered.should contain('100')
#   end
# end

require "rails_helper"

  RSpec.describe "doctors/show" do
    it "displays doctors details correctly" do
      doctor = Doctor.create!(:name => 'xyz',:experience =>'5',:fee =>'100')
      controller.extra_params = { :id => doctor.id }
      expect(controller.request.fullpath).to eq doctor_path(doctor)
    end
end
