require "rails_helper"

RSpec.describe "doctor/edit" do
    it "edit doctor details" do
      @doctor = assign(:doctors, [Doctor.create!(:name => 'xyz',:experience =>'5',:fee =>'100')])
      render
      expect(rendered).to match /xyz/
      expect(rendered).to match /5/
      expect(rendered).to match /100/
  end
end