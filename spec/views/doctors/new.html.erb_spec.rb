require "rails_helper"

RSpec.describe "doctors/new" do
  before(:each) do
      assign(:doctor, [ Doctor.create!(:name => 'xyz',:experience =>'5',:fee =>'100')
      ])
  end

  it "renders a form to create doctor" do
    render
    rendered.should have_selector("form", :method => "post", :action => doctors_path) do
      |form|
      form.should have_selector("input", :type => "submit")
    end
  end
end