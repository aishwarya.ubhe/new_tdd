require "rails_helper"

RSpec.describe "doctors/index" do
  it "displays all the doctors" do
    assign(:doctors, [
      Doctor.create!(:name => 'xyz',:experience =>'5',:fee =>'100')
    ])
    render

    expect(rendered).to match /xyz/
    expect(rendered).to match /1/
    expect(rendered).to match /10/
  end
end