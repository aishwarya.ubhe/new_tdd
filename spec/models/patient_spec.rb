require 'rails_helper'

RSpec.describe Patient, type: :model do
  context 'validation tests' do

    it 'ensure patient name present' do
      patient = Patient.new(name: 'xyz').save
        expect(patient).to eq(false)
    end
    it 'ensure gender present' do
       patient = Patient.new(gender: 'Male').save
        expect(patient).to eq(false)
    end

    it 'ensure email present' do
       patient = Patient.new(email: 'abc@bc.com').save
        expect(patient).to eq(false)
    end

    it 'ensure age present' do
       patient = Patient.new(age: '30').save
        expect(patient).to eq(false)
    end

    it 'should save successfully' do
        patient = Patient.new(name: 'xyz',gender:'Male',email: 'abc@bc.com', age: '30').save
        expect(patient).to eq(true)
    end

    context 'scope tests' do
    end

  end


end
