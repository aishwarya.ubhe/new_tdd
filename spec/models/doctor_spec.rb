require 'rails_helper'

RSpec.describe Doctor, type: :model do
  context 'validation tests' do

    it 'ensure doctor name present' do
      doctor = Doctor.new(name: 'xyz').save
        expect(doctor).to eq(false)
    end
    it 'ensure experience present' do
       doctor = Doctor.new(experience: '5').save
        expect(doctor).to eq(false)
    end

    it 'ensure fee present' do
       doctor = Doctor.new(fee: '100').save
        expect(doctor).to eq(false)
    end

    it 'should save successfully' do
        doctor = Doctor.new(name: 'xyz',experience:'5',fee: '100').save
        expect(doctor).to eq(true)
    end

  end

  context 'scope tests' do

    let(:params){{name: 'xyz',experience:'5',fee: '100'}}

    before(:each) do
      Doctor.new(params).save
      Doctor.new(params).save
      Doctor.new(params.merge({availability: false})).save
      Doctor.new(params.merge({availability: true})).save
      Doctor.new(params.merge({availability: true})).save
      end

      it 'should return available doctors' do
        expect(Doctor.available_doctors.size).to eq(2)
      end

      it 'should return unavailable doctors' do
        expect(Doctor.unavailable_doctors.size).to eq(1)
      end
   end
end
